// const config = require('../config');
const logger = require('../utils/logger');

const messageUtil = require('../utils/messageUtil');
// const facebookUtil = require('../utils/facebookUtil');
const kyaungkyoUtil = require('../utils/kyaungkyoUtil');

const GET_STARTED_RESPONSE = 'Please click menu "Traffic Update"'+
  ' to view traffic update near Tharyar Shwe Pyi ILBC preschool';

const TRAFFIC_TEXT_WITH_LEVEL = function (level) {
  let trafficText = 'Traffic Level near Tharyar Shwe Pyi ILBC Preschool: ';
  switch(level) {
  case 0:
    trafficText += 'Low';
    break;
  case 1:
    trafficText += 'Moderate';
    break;
  case 2:
    trafficText += 'High';
    break;
  default:
    trafficText += 'Unknown';
    break;
  }
  return trafficText;
};

class MessageWebhook {

  static processMessage(event) {
    const senderId = event.sender.id;
    // const message = event.message.text;
    messageUtil.sendTextMessage(senderId, GET_STARTED_RESPONSE);
  }

  static processPostback(event) {
    const senderId = event.sender.id;
    const payload = event.postback.payload;
    logger.debug(`sender id ${senderId} payload ${payload}`);

    if (payload === 'Get Started') {
      logger.debug('get started');
      messageUtil.sendTextMessage(senderId, GET_STARTED_RESPONSE);
    } else if (payload === 'traffic_update') {
      logger.debug('traffic update');
      kyaungkyoUtil.getTraffic().then( level => {
        messageUtil.sendTextMessage(senderId, TRAFFIC_TEXT_WITH_LEVEL(level));
      });
    }
  }
}

module.exports = {
  MessageWebhook: MessageWebhook
};