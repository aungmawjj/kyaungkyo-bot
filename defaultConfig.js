// create a file 'config.js' with following contents
// modify FACEBOOK_ACCESS_TOKEN and BOT_VERIFY_TOKEN

const config = {};

config.host = '0.0.0.0';
config.port = 9099;

config.logFile = './log/kyaungkyo-bot.log';
config.logLevel = 'debug';
// error: 0, 
// warn: 1, 
// info: 2, 
// verbose: 3, 
// debug: 4, 
// silly: 5

config.BOT_VERIFY_TOKEN = 'bot_verify_token';
config.FACEBOOK_ACCESS_TOKEN = 'your_token';

module.exports = config;