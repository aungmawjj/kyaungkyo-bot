const config = require('./config');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

app.use(cors({
  'origin': true,
  'credentials': true,
}));

app.use(morgan('combined'));

app.use('/' , require('./controllers'));

app.listen(config.port, config.host, () => {
  console.log('Api listening at port:', config.port);
});