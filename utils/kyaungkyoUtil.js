const fetch = require('isomorphic-fetch');

module.exports.getTraffic = function () {
  const url = 'http://kaa.ninja:9090/trafficLevel';
  const options = {
    method: 'GET'
  };
  return fetch(url, options)
    .then(response => response.json());
};