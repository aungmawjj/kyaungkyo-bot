const config = require('../config');
const fetch = require('isomorphic-fetch');

module.exports.getUser = function (id) {
  const url = `https://graph.facebook.com/v3.0/${id}`;
  const qs = `?access_token=${config.FACEBOOK_ACCESS_TOKEN}`;
  const options = {
    method: 'GET'
  };
  return fetch(url + qs, options)
    .then(response => response.json());
};