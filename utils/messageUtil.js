const config = require('../config');
const fetch = require('isomorphic-fetch');

module.exports.sendTextMessage = function (senderId, text) {
  const url = 'https://graph.facebook.com/v2.6/me/messages';
  const qs = `?access_token=${config.FACEBOOK_ACCESS_TOKEN}`;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      recipient: { id: senderId },
      message: { text },
    })
  };
  return fetch(url + qs, options)
    .then(response => response.json());
};