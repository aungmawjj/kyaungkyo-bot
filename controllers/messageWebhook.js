const config = require('../config');
const logger = require('../utils/logger');
const express = require('express');
const { MessageWebhook } = require('../models/messageWebhook');
const router = express.Router();

router.get('/', (req, res) => {
  logger.debug('verification');
  const hubChallenge = req.query['hub.challenge'];
  const hubMode = req.query['hub.mode'];
  const verifyTokenMatches =
  (req.query['hub.verify_token'] === config.BOT_VERIFY_TOKEN);
  if (hubMode && verifyTokenMatches) {
    res.status(200).send(hubChallenge);
  } else {
    res.status(403).end();
  }
});

router.post('/', (req, res) => {
  if (req.body.object === 'page') {
    req.body.entry.forEach(entry => {
      entry.messaging.forEach(event => {
        logger.debug(JSON.stringify(event));
        if (event.message && event.message.text) {
          MessageWebhook.processMessage(event);
        } else if (event.postback) {
          MessageWebhook.processPostback(event);
        }
      });
    });
    res.status(200).end();
  }
});

module.exports = router;
